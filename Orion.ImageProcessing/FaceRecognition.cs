﻿using Neurotec.Biometrics.Client;
using Neurotec.Biometrics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neurotec.Licensing;

namespace Orion.ImageProcessing
{
    public class FaceRecognition
    {
        private string sPathToImages = System.Configuration.ConfigurationManager.AppSettings["PathToImages"];
        private string sPathToTemplates = System.Configuration.ConfigurationManager.AppSettings["PathToTemplates"];
        private string sLicenceServer = System.Configuration.ConfigurationManager.AppSettings["LicenceServer"];
        private Recognizer _recognizer;

        public FaceRecognition()
        {
            if (!NLicense.ObtainComponents(sLicenceServer, 5000, "Biometrics.FaceExtraction,Biometrics.FaceMatching,Biometrics.FaceDetection,Devices.Cameras,Biometrics.FaceSegmentsDetection"))
            {
                throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Biometrics.FaceExtraction,Biometrics.FaceMatching,Biometrics.FaceDetection,Devices.Cameras,Biometrics.FaceSegmentsDetection"));
            }
            _recognizer = new Recognizer();
        }
        public bool SaveFace(int iPatientId, int iPictureId, Image image)
        {            
            try
            {
                //if (!PictureQuality.CheckPicture(image))
                //    return false;
                Guid guid;
                guid = Guid.NewGuid();
                string facename = iPatientId.ToString() + "_" + "pic" + iPictureId.ToString() + "_" + guid.ToString() + ".jpg";
                if (Directory.Exists(sPathToImages))
                {
                    image.Save(Path.Combine(sPathToImages, facename), ImageFormat.Jpeg);
                }
                else
                {
                    throw new Exception(string.Format("Folder {0} does not exists!", sPathToImages));
                }
                //RefreshTemplate(iPatientId);
            }
            catch
            {
                throw;
            }
            return true;
        }
        public bool SaveFace(int iPatientId, int iPictureId, List<Image> image)
        {
            //Trainer tr = new Trainer();s
            //tr.SaveFace(image, iPatientId);
            foreach (var i in image)
            {
                SaveFace(iPatientId, iPictureId, i);
            }
            return true;

        }
        public void RefreshTemplate()
        {
            List<string> result = new List<string>();
            DirectoryInfo d = new DirectoryInfo(sPathToImages);
            foreach (var f in d.EnumerateFiles())
            {
                string[] fileinfo = f.Name.Split('_');
                result.Add(fileinfo[0]);
            }
            var x = result.Distinct();
            
            foreach (var a in x)
            {
                RefreshTemplate(Convert.ToInt32(a));
            }
        }
        public void RefreshTemplate(int iPatientId)
        {
            NBiometricClient tempBiometricClient = new NBiometricClient();
            tempBiometricClient = new NBiometricClient { BiometricTypes = NBiometricType.Face };
            tempBiometricClient.Initialize();
            EnrollFromImage x = new EnrollFromImage();
            x.BiometricClient = tempBiometricClient;
            x.RefreshTemplate(iPatientId);
            
        }
        public void ReTrainRecognizer()
        {
            //check if templates are missing
            List<string> lsImages = Directory.GetFiles(sPathToImages, "*.jpg").ToList<string>();
            List<string> lsTemplates = Directory.GetFiles(sPathToTemplates, "*.template").ToList<string>();
            List<string> lsPatients = new List<string>();
            foreach (var image in lsImages)
            {
                string Fimage = Path.GetFileName(image);
                string sPat = Fimage.Split('_')[0];
                if(!lsPatients.Contains(sPat))
                {
                    lsPatients.Add(sPat);
                    if (!File.Exists(sPathToTemplates+"\\"+sPat+".template"))
                    {
                        RefreshTemplate(Convert.ToInt32(sPat));
                    }
                }
            }
            //reload templates
            _recognizer.ReloadTemplates();
        }
        
        public RecognitionResult RecognizeFace(Image image)
        {
            return _recognizer.Recognize(image);
           
        }

        public bool DeletePictureFromDB(int iPatientId, int iPictureId)
        {
            string tmp = iPatientId.ToString() + "_pic" + iPictureId.ToString() + "_*.*";
            List<string> fileNames = Directory.GetFiles(sPathToImages, tmp).ToList<string>();
            string templateFilename = Path.Combine(sPathToTemplates, iPatientId.ToString() + ".template");
            
            try
            {
                foreach (var f in fileNames)
                {
                    File.Delete(f);
                }
                File.Delete(templateFilename);
                return true;
            }
            catch { throw; }
            return false;
        }
        //public void GetImages(int iPatientId)
        //{
        //    Trainer tr = new Trainer();
        //    tr.GetImages(iPatientId);
        //}
    }
}
