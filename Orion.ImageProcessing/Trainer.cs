﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using System.Drawing;
using Neurotec.Media;
using Neurotec.Images;
using System.IO;
using Neurotec.Licensing;

namespace Orion.ImageProcessing
{
    /// <summary>
    /// job of trainer is to receive image and sve it to coresponding NSubject
    /// </summary>
    class Trainer
    {
        private string sPathToSubjects = @"C:\temp\train";
        public void SaveFace(Image image, int iSubjectId)
        {
            if (!NLicense.ObtainComponents("/local", 5000, "Biometrics.FaceExtraction"))
            {
                throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Components"));
            }
            NImage nImg;
            nImg = NImage.FromBitmap(new Bitmap(image));
            string sSubjectFilename = Path.Combine(sPathToSubjects, iSubjectId.ToString() + ".subject");
            var subject = new NSubject();
            using (var biometricClient = new NBiometricClient())
            {
                subject.Id = iSubjectId.ToString();
                biometricClient.FacesTemplateSize = NTemplateSize.Large;
                // Detect all faces features
                biometricClient.FacesDetectAllFeaturePoints = true;
                //check if subject exists
                if (File.Exists(sSubjectFilename))
                {
                    //add to current subject
                    subject.SetTemplateBuffer(Neurotec.IO.NBuffer.FromArray(File.ReadAllBytes(sSubjectFilename)));
                }
                NFace face = new NFace();
                face.Image = nImg;
                subject.Faces.Add(face);
                var status = biometricClient.CreateTemplate(subject);
                File.WriteAllBytes(sSubjectFilename, subject.GetTemplateBuffer().ToArray());
                
            }
        }
        public void SaveFace(List<Image> image, int iSubjectId)
        {
            if (!NLicense.ObtainComponents("/local", 5000, "Biometrics.FaceExtraction"))
            {
                throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Components"));
            }
            
            
            string sSubjectFilename = Path.Combine(sPathToSubjects, iSubjectId.ToString() + ".subject");
            var subject = new NSubject();
            using (var biometricClient = new NBiometricClient())
            {
                subject.Id = iSubjectId.ToString();
                biometricClient.FacesTemplateSize = NTemplateSize.Large;
                // Detect all faces features
                biometricClient.FacesDetectAllFeaturePoints = true;
                //check if subject exists
                if (File.Exists(sSubjectFilename))
                {
                    //add to current subject
                    subject.SetTemplateBuffer(Neurotec.IO.NBuffer.FromArray(File.ReadAllBytes(sSubjectFilename)));
                }
                foreach (var img in image)
                {
                    NFace face = new NFace();
                    NImage nImg;
                    nImg = NImage.FromBitmap(new Bitmap(img));
                    face.Image = nImg;
                    subject.Faces.Add(face);
                }
                NBiometricTask enrollTask = biometricClient.CreateTask(NBiometricOperations.CreateTemplate, subject);
                biometricClient.PerformTask(enrollTask);
                //var status = biometricClient.CreateTemplate(subject);
                File.WriteAllBytes(sSubjectFilename, subject.GetTemplateBuffer().ToArray());

            }
        }
        //neradi
        public void GetImages(int iSubjectId)
        {
            if (!NLicense.ObtainComponents("/local", 5000, "Biometrics.FaceExtraction"))
            {
                throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Components"));
            }
            string sSubjectFilename = Path.Combine(sPathToSubjects, iSubjectId.ToString() + ".subject");
            var subject = new NSubject();
            subject.SetTemplateBuffer(Neurotec.IO.NBuffer.FromArray(File.ReadAllBytes(sSubjectFilename)));
            int i = 0;
            int x = subject.Faces.Count();
            foreach (NFace f in subject.Faces)
            {
                f.Image.Save(i.ToString(), NImageFormat.Bmp);
                i++;
            }
        }
            
        
    }
}
