﻿using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Images;
using Neurotec.Licensing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orion.ImageProcessing
{
    class Recognizer
    {

        #region Public constructor

        public Recognizer()
        {
            if (!NLicense.ObtainComponents("/local", 5000, "Biometrics.FaceExtraction,Biometrics.FaceMatching"))
            {
                throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Components"));
            }
            _biometricClient = new NBiometricClient { BiometricTypes = NBiometricType.Face };
            _biometricClient.Initialize();
            if (LoadTemplates())
                _TemplatesLoaded = true;            
        }

        public RecognitionResult Recognize(Image image)
        {
            LoadProbeImage(image);
            return IdentifyImage();
            //for debug use no exception
            //if(LoadProbeImageNoException(image))            
            //    return IdentifyImage();
            //RecognitionResult res = new RecognitionResult();
            //res.Id = -1;
            //return res;
        }

        public bool ReloadTemplates()
        {
            _TemplatesLoaded = false;
            _TemplatesLoaded = LoadTemplates();
            return _TemplatesLoaded;
        }
        #endregion

        #region Private fields

        private NBiometricClient _biometricClient;
        private NSubject _subject;
        private NSubject[] _subjects;
        private string sPathToTemplates = System.Configuration.ConfigurationManager.AppSettings["PathToTemplates"];
        private bool _TemplatesLoaded = false;
        private List<string> TemplatesFileNames
        {
            get
            {
                List<string> result = new List<string>();
                DirectoryInfo d = new DirectoryInfo(sPathToTemplates);
                foreach (var f in d.EnumerateFiles())
                {
                    result.Add(f.FullName);
                }
                return result;
            }
        }
        #endregion

        #region Public properties

        public NBiometricClient BiometricClient
        {
            get { return _biometricClient; }
            set { _biometricClient = value; }
        }

        #endregion

        #region Private methods

        //private void OnEnrollCompleted(IAsyncResult r)
        //{

        //    NBiometricTask task = _biometricClient.EndPerformTask(r);
        //    if (task.Status == NBiometricStatus.Ok)
        //    {
        //        _TemplatesLoaded = true;
        //    }
        //    else throw new Exception(string.Format("Enrollment failed: {0}", task.Status));

        //}

        //private void OnIdentifyDone(IAsyncResult r)
        //{
        //    NBiometricStatus status = _biometricClient.EndIdentify(r);
        //    if (status == NBiometricStatus.Ok || status == NBiometricStatus.MatchNotFound)
        //    {
        //        // Matching subjects
        //        foreach (var result in _subject.MatchingResults)
        //        {
        //            listView.Items.Add(new ListViewItem(new[] { result.Id, result.Score.ToString(CultureInfo.InvariantCulture) }));
        //        }
        //        foreach (var subject in _subjects)
        //        {
        //            bool isMatchingResult = _subject.MatchingResults.Any(result => subject.Id == result.Id);
        //            if (!isMatchingResult)
        //                listView.Items.Add(new ListViewItem(new [] { subject.Id, "0" }));
        //        }
				
        //        else MessageBox.Show(string.Format("Identify failed: {0}", status));
        //    }
        //}

        //private void OnExtractionCompleted(IAsyncResult r)
        //{
            
        //    NBiometricStatus status = _biometricClient.EndCreateTemplate(r);
        //    if (status != NBiometricStatus.Ok)
        //    {
        //        MessageBox.Show(string.Format("Template was not extracted: {0}.", status), Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        _subject = null;
        //        identifyButton.Enabled = false;
        //    }
        //    else
        //    {
        //        identifyButton.Enabled = _subjects != null;
        //    }
            
        //}

        #endregion

        #region Private form events

        private bool LoadTemplates()
        {
            try
            {
                List<string> lsTempTemplates = TemplatesFileNames;
                if (lsTempTemplates.Count < 1)
                    //throw new Exception("No templates to load!");
                    return false;
                _subjects = new NSubject[lsTempTemplates.Count];
                // Create subjects from selected templates
                for (int i = 0; i < lsTempTemplates.Count; i++)
                {
                    _subjects[i] = NSubject.FromFile(lsTempTemplates[i]);
                    _subjects[i].Id = Path.GetFileNameWithoutExtension(lsTempTemplates[i]);
                }
                // Clean earlier data before proceeding, enroll new data
                _biometricClient.Clear();
                // Create enrollment task
                var enrollmentTask = new NBiometricTask(NBiometricOperations.Enroll);
                // Create subjects from templates and set them for enrollment
                foreach (NSubject t in _subjects)
                {
                    enrollmentTask.Subjects.Add(t);
                    //_biometricClient.Enroll(t);
                }
                // Enroll subjects
                //_biometricClient.BeginPerformTask(enrollmentTask, OnEnrollCompleted, null);
                _biometricClient.PerformTask(enrollmentTask);
                if (enrollmentTask.Status == NBiometricStatus.Ok)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                throw;
            }

        }

        private void LoadProbeImage(Image ProbeImage)
        {
            _subject = null;            
            try
            {
                // Create a face object
                NImage NProbeImage = NImage.FromBitmap(new Bitmap(ProbeImage));
                var face = new NFace {Image = NProbeImage };
                // Add the face to a subject
                _subject = new NSubject();
                _subject.Faces.Add(face);
                // Extract a template from the subject
                //_biometricClient.BeginCreateTemplate(_subject, OnExtractionCompleted, null);
                _biometricClient.CreateTemplate(_subject);
                NBiometricStatus status = _biometricClient.CreateTemplate(_subject);
                if (status != NBiometricStatus.Ok)
                {
                    _subject = null;
                    throw new Exception(string.Format("Template was not extracted: {0}.", status));                    
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error {0}", ex));
            }
            
        }

        private bool LoadProbeImageNoException(Image ProbeImage)
        {
            _subject = null;
            try
            {
                // Create a face object
                NImage NProbeImage = NImage.FromBitmap(new Bitmap(ProbeImage));
                var face = new NFace { Image = NProbeImage };
                // Add the face to a subject
                _subject = new NSubject();
                _subject.Faces.Add(face);
                // Extract a template from the subject
                //_biometricClient.BeginCreateTemplate(_subject, OnExtractionCompleted, null);
                _biometricClient.CreateTemplate(_subject);
                NBiometricStatus status = _biometricClient.CreateTemplate(_subject);
                if (status != NBiometricStatus.Ok)
                {
                    _subject = null;
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error {0}", ex));
            }

        }

        private RecognitionResult IdentifyImage()
        {
            try
            {
                // Identify current subject with enrolled ones
                //_biometricClient.BeginIdentify(_subject, OnIdentifyDone, null);                
                NBiometricStatus status = _biometricClient.Identify(_subject);
                if (status == NBiometricStatus.Ok || status == NBiometricStatus.MatchNotFound)
                {
                    // Matching subjects
                    List<string> mRes = new List<string>();
                    foreach (var result in _subject.MatchingResults)
                    {
                        mRes.Add(string.Format("Matched with ID: '{0}' with score {1}", result.Id, result.Score));
                        
                    }
                    RecognitionResult res = new RecognitionResult { Confidence = 0, Id = -1 };
                    if (mRes.Count == 0)
                        return res;
                    res.MatchResults = mRes;
                    var maxRes = _subject.MatchingResults.Max(s => s.Score);
                    var reeees = _subject.MatchingResults.FirstOrDefault(x => x.Score == maxRes);
                    res.Confidence = maxRes;
                    res.Id = Convert.ToInt32(reeees.Id);
                    return res;
                }
                else throw new Exception(string.Format("Identify failed: {0}", status));
            }
            catch
            {
                throw;
            }
            
        }
        
        #endregion
    }
}
