﻿using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Images;
using Neurotec.Licensing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orion.ImageProcessing
{
    class Matcher
    {
        private string sPathToSubjects = @"C:\temp\train";
        public RecognitionResult Recognize (Image image)
        {
            RecognitionResult res = new RecognitionResult();
            if (!NLicense.ObtainComponents("/local", 5000, "Biometrics.FaceExtraction,Biometrics.FaceMatching"))
            {
                throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Components"));
            }

            using (var biometricClient = new NBiometricClient())
            {
                // Create probe template
                biometricClient.FacesMaximalRoll = 35;
                biometricClient.FacesMaximalYaw = 35;
                biometricClient.FacesMinimalInterOcularDistance = 35;
                NSubject probeSubject = CreateSubject(image);
                NBiometricStatus status = biometricClient.CreateTemplate(probeSubject);
                //if (status != NBiometricStatus.Ok)
                //{
                //    throw new Exception("Failed to create probe template!");
                //}

                // Create gallery templates and enroll them
                NBiometricTask enrollTask = biometricClient.CreateTask(NBiometricOperations.Enroll, null);
                //debug
                DirectoryInfo d = new DirectoryInfo(sPathToSubjects);
                
                foreach (var f in d.EnumerateFiles())
                {
                    var tmpSubject = new NSubject();
                    tmpSubject.SetTemplateBuffer(Neurotec.IO.NBuffer.FromArray(File.ReadAllBytes(f.FullName)));
                    tmpSubject.Id = f.Name.Substring(0, 1);
                    enrollTask.Subjects.Add(tmpSubject);
                }

                biometricClient.PerformTask(enrollTask);
                status = enrollTask.Status;
                if (status != NBiometricStatus.Ok)
                {
                    if (enrollTask.Error != null) throw enrollTask.Error;
                    throw new Exception("Enrolment unsuccesful!");
                }

                // Set matching threshold
                biometricClient.MatchingThreshold = 48;

                // Set matching speed
                biometricClient.FacesMatchingSpeed = NMatchingSpeed.Low;

                // Identify probe subject
                status = biometricClient.Identify(probeSubject);
                List<string> mRes = new List<string>();
                if (status == NBiometricStatus.Ok)
                {
                    foreach (var matchingResult in probeSubject.MatchingResults)
                    {
                        mRes.Add(string.Format("Matched with ID: '{0}' with score {1}", matchingResult.Id, matchingResult.Score));
                    }
                }
                else
                {
                    if (enrollTask.Error != null) throw enrollTask.Error;
                }
                if (mRes.Count == 0)
                    return res;
                res.MatchResults = mRes;
                var maxRes = probeSubject.MatchingResults.Max(s => s.Score);
                var reeees = probeSubject.MatchingResults.FirstOrDefault(x => x.Score == maxRes);
                res.Confidence = maxRes;
                res.Id = Convert.ToInt32(reeees.Id);
            }
            
            return res;
        }

        private NSubject CreateSubject(Image img)
        {
            NImage nImg;
            Guid g;
            g = Guid.NewGuid();
            nImg = NImage.FromBitmap(new Bitmap(img));
            var subject = new NSubject();
            NFace face = new NFace();
            face.Image = nImg;
            subject.Faces.Add(face);
            subject.Id = g.ToString();
            return subject;
        }
    }
}
