﻿using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Licensing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orion.ImageProcessing
{
    public partial class EnrollFromImage
    {
        #region Public constructor

        /// <summary>
        /// Enrolls images for ID
        /// </summary>
        public void RefreshTemplate(int iSubjectId)
        {
            id = iSubjectId;
           
            //get all images for template
            _lsImages = lsImages;
            if (_lsImages.Count == 0)
                return;
            
            //add images to subject
            AddImagesToSubject();
            //extract and save template
            ExtractAndSaveTemplate();
            //for debuging to wait task
            //while (working);
        }

        public EnrollFromImage()
        {
            //if (!NLicense.ObtainComponents("/local", 5000, "Biometrics.FaceExtraction,Biometrics.FaceMatching,Biometrics.FaceDetection,Devices.Cameras,Biometrics.FaceSegmentsDetection"))
            //{
            //    throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Components"));
            //}            
        }
        #endregion

        #region Private fields

        private NBiometricClient _biometricClient = new NBiometricClient();
        private NSubject _subject;
        private string sPathToImages = System.Configuration.ConfigurationManager.AppSettings["PathToImages"];
        private string sPathToTemplates = System.Configuration.ConfigurationManager.AppSettings["PathToTemplates"];
        private int id;
        private List<string> _lsImages;
        private bool working = true;
        #endregion

        #region Public properties

        public NBiometricClient BiometricClient
        {
            get { return  _biometricClient; }
            set { _biometricClient = value; }
        }

        #endregion

        #region Private properties
        
        private string sTemplateFile
        {
            get {return Path.Combine(sPathToTemplates,id.ToString()+ ".template");}
        }

        private List<string> lsImages
        {
            get
            {
                List<string> result = new List<string>();
                DirectoryInfo d = new DirectoryInfo(sPathToImages);
                foreach (var f in d.EnumerateFiles())
                {
                    string[] fileinfo = f.Name.Split('_');
                    if (fileinfo[0] == id.ToString())
                        result.Add(f.FullName);
                }
                return result;
            }
        }

        #endregion

        #region Private methods

        private void SetBiometricClientParams()
        {
            _biometricClient.FacesDetectAllFeaturePoints = true;
            _biometricClient.FacesDetectBaseFeaturePoints = true;
            _biometricClient.FacesDetermineGender = true;
            _biometricClient.FacesDetectProperties = true;
            _biometricClient.FacesRecognizeEmotion = true;
            _biometricClient.FacesRecognizeExpression = true;
            _biometricClient.FacesTemplateSize = NTemplateSize.Large;
        }

        private void ExtractAndSaveTemplate()
        {
            if (_subject == null) return;
            SetBiometricClientParams();

            // Extract template
            //_biometricClient.BeginCreateTemplate(_subject, OnExtractDone, null);
            var status = _biometricClient.CreateTemplate(_subject);
            if (status == NBiometricStatus.Ok)
            {
                SaveTemplate();
            }else
            {
                throw new Exception(string.Format("Extraction failed: {0}", status));
            }
        }

        private void OnExtractDone(IAsyncResult r)
        {
            NBiometricStatus status = _biometricClient.EndCreateTemplate(r);
            if (status == NBiometricStatus.Ok)
            {
                SaveTemplate();
            }
            else
            {
                //debug throw new Exception(string.Format("Extraction failed: {0}", status));
            }
            working = false;
        }

        #endregion

        #region Private events               

        private void SaveTemplate()
        {
            
            try
            {
                File.WriteAllBytes(sTemplateFile, _subject.GetTemplateBuffer().ToArray());
            }
            catch
            {
                throw new Exception("Could not save template!");
            }
            
        }

        private void AddImagesToSubject()
        {
            try
            {
                
                // Create new subject
                _subject = new NSubject();
                foreach (string s in _lsImages)
                {
                    var face = new NFace { FileName = s };
                    _subject.Faces.Add(face);
                }
                _subject.Id = id.ToString();                
                ExtractAndSaveTemplate();
                
            }
            catch
            {
                throw;
            }
        }
        

        #endregion
    }

}
