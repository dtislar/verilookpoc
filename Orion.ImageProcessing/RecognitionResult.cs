﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orion.ImageProcessing
{
    public class RecognitionResult
    {
        public int Id { get; set; }
        public decimal Confidence { get; set; }
        public List<string> MatchResults { get; set; }
    }
}
