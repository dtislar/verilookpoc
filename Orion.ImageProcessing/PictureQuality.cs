﻿using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Images;
using Neurotec.Licensing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orion.ImageProcessing
{
    public static class PictureQuality
    {
        private static string sLicenceServer = System.Configuration.ConfigurationManager.AppSettings["LicenceServer"];

        public static bool CheckPicture(Image image)
        {
            if (!NLicense.ObtainComponents(sLicenceServer, 5000, "Biometrics.FaceExtraction"))
            {
                throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Components"));
            }
            using (var biometricClient = new NBiometricClient())
            {
                // Create probe template
                NSubject probeSubject = CreateSubject(image);
                NBiometricStatus status = biometricClient.CreateTemplate(probeSubject);
                biometricClient.Clear();
                biometricClient.Dispose();
                if (status != NBiometricStatus.Ok)
                {
                    return false;
                }
            }


            return true;
        }
        public static bool DoesContainFace(ref Image image)
        {
            if (!NLicense.ObtainComponents(sLicenceServer, 5000, "Biometrics.FaceExtraction,Biometrics.FaceMatching,Biometrics.FaceDetection,Devices.Cameras,Biometrics.FaceSegmentsDetection"))
            {
                throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Components"));
            }
            using (var biometricClient = new NBiometricClient())
            {
                // Create probe template
                NSubject probeSubject = CreateSubject(image);
                biometricClient.FacesDetectAllFeaturePoints = false;
                biometricClient.FacesDetectBaseFeaturePoints = true;
                biometricClient.FacesDetermineGender = false;
                biometricClient.FacesDetectProperties = false;
                biometricClient.FacesRecognizeEmotion = false;
                biometricClient.FacesRecognizeExpression = false;
                biometricClient.FacesTemplateSize = NTemplateSize.Large;
                NBiometricStatus status = biometricClient.CreateTemplate(probeSubject);
                return status == NBiometricStatus.Ok;
            }
        }
        public static void GetPictureFeatures(ref Image image)
        {
            if (!NLicense.ObtainComponents(sLicenceServer, 5000, "Biometrics.FaceExtraction,Biometrics.FaceMatching,Biometrics.FaceDetection,Devices.Cameras,Biometrics.FaceSegmentsDetection"))
            {
                throw new ApplicationException(string.Format("Could not obtain licenses for components: {0}", "Components"));
            }
            using (var biometricClient = new NBiometricClient())
            {
                // Create probe template
                NSubject probeSubject = CreateSubject(image);
                biometricClient.FacesDetectAllFeaturePoints = true;
                biometricClient.FacesDetectBaseFeaturePoints = true;
                biometricClient.FacesDetermineGender = true;
                biometricClient.FacesDetectProperties = true;
                biometricClient.FacesRecognizeEmotion = true;
                biometricClient.FacesRecognizeExpression = true;
                biometricClient.FacesTemplateSize = NTemplateSize.Large;
                NBiometricStatus status = biometricClient.CreateTemplate(probeSubject);

                if (status == NBiometricStatus.Ok)
                {
                    Bitmap newImage = probeSubject.Faces[0].Image.ToBitmap();

                    var leftEye = probeSubject.Faces[0].Objects[0].LeftEyeCenter;
                    var rightEye = probeSubject.Faces[0].Objects[0].RightEyeCenter;
                    //probeSubject.Faces[0].Objects[0].
                    Graphics g = Graphics.FromImage(newImage);
                    Pen greenPen = new Pen(Color.LawnGreen);
                    int iElipWidt = (int)(newImage.Width * 0.025);
                    greenPen.Width = (float)(iElipWidt * 0.15);
                    //eye circles
                    g.DrawEllipse(greenPen, leftEye.X - (iElipWidt / 2), leftEye.Y - (iElipWidt / 2), iElipWidt, iElipWidt);
                    g.DrawEllipse(greenPen, rightEye.X - (iElipWidt / 2), rightEye.Y - (iElipWidt / 2), iElipWidt, iElipWidt);
                    //eye line + pixels
                    //g.DrawLine(greenPen, (((leftEye.X - rightEye.X) / 3) * 2 + rightEye.X), (((leftEye.Y - rightEye.Y) / 3 * 2) + rightEye.Y), leftEye.X - (iElipWidt / 2)+2, leftEye.Y);
                    //g.DrawLine(greenPen, rightEye.X + (iElipWidt / 2)+2, rightEye.Y, (((leftEye.X - rightEye.X) / 3) + rightEye.X), (((leftEye.Y - rightEye.Y) / 3) + rightEye.Y));
                    g.DrawLine(greenPen, rightEye.X + (iElipWidt / 2)+2, rightEye.Y, leftEye.X - (iElipWidt / 2) + 2, leftEye.Y);
                    int dist = ((int)Math.Sqrt(Math.Pow(Math.Abs(leftEye.X - rightEye.X), 2) + Math.Pow(Math.Abs(leftEye.Y - rightEye.Y), 2)));
                    //g.DrawString(dist, new Font("Arial", iElipWidt, FontStyle.Bold), new SolidBrush(Color.LawnGreen), (((leftEye.X - rightEye.X) / 3) + rightEye.X), (((leftEye.Y - rightEye.Y) / 3) + rightEye.Y ));                    
                    //feature dots
                    foreach (var d in probeSubject.Faces[0].Objects[0].FeaturePoints)
                    {
                        if(d.Confidence > 0)
                            g.DrawEllipse(greenPen, d.X, d.Y,iElipWidt*0.1f,iElipWidt*0.1f);
                    }
                    //recognized features
                    List<string> lsFeatures = new List<string>();
                    //lsFeatures.Add(String.Format("{0} - {1}", "Feature", "Confidence"));
                    //gender
                    lsFeatures.Add(frmStr("Eyes distance",dist));
                    var pObject = probeSubject.Faces[0].Objects[0];
                    if (pObject.GenderConfidence > 0)
                        lsFeatures.Add(frmStr(pObject.Gender.ToString(),pObject.GenderConfidence));
                    if (pObject.ExpressionConfidence > 0 && pObject.Expression.ToString() != "Unknown")
                        lsFeatures.Add(frmStr(pObject.Expression.ToString(), pObject.ExpressionConfidence));
                    //if (pObject.BlinkConfidence > 0)
                    //    lsFeatures.Add(frmStr("Blink", pObject.BlinkConfidence));
                    //if (pObject.MouthOpenConfidence > 0)
                    //    lsFeatures.Add(frmStr("Mouth open", pObject.MouthOpenConfidence));
                    if(pObject.GlassesConfidence>75)
                        lsFeatures.Add(frmStr("Glasses", pObject.GlassesConfidence));
                    lsFeatures.Add(frmStr("Sharpness",pObject.Sharpness));
                    lsFeatures.Add(frmStr("Quality", pObject.Quality));

                    int iSy = 1;
                    foreach (var s in lsFeatures)
                    {
                        //g.DrawString(s, new Font(FontFamily.GenericSansSerif, iElipWidt * 2), new SolidBrush(Color.Red), 1, iSy);
                        GraphicsPath p = new GraphicsPath();
                        p.AddString(s,FontFamily.GenericSansSerif, (int)FontStyle.Regular, g.DpiY * (float)(iElipWidt * 2.5) / 72, new Point(0, iSy), new StringFormat());
                        g.FillPath(Brushes.Black, p);
                        g.DrawPath(Pens.White, p);
                        iSy = iSy + (int)(iElipWidt*2.7);
                    }
                    image = (Image)newImage;
                }
                else
                {
                    //return bad status
                    Bitmap newImage = (Bitmap)image;
                    Graphics g = Graphics.FromImage(newImage);
                    int iElipWidt = (int)(newImage.Width * 0.025);
                    g.DrawString(status.ToString() , new Font("Arial", iElipWidt * 2), new SolidBrush(Color.Red), 1, 1);
                }
            }
        }
        private static NSubject CreateSubject(Image img)
        {
            NImage nImg;
            Guid g;
            g = Guid.NewGuid();
            nImg = NImage.FromBitmap(new Bitmap(img));
            var subject = new NSubject();
            NFace face = new NFace();
            face.Image = nImg;
            subject.Faces.Add(face);
            subject.Id = g.ToString();
            return subject;
        }
        private static string frmStr(String s, int conf)
        {
            return String.Format("{0} - {1}", s, conf.ToString());
        }
    }
}
