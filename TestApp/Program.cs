﻿using Orion.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TestApp
{
    class Program
    {
        static void Main()
        {
            //string fName = @"c:\FaceDB\i621yd-fn.jpg";
            //Stopwatch sw = Stopwatch.StartNew();
            //FaceRecognition fr = new FaceRecognition();
            
            //fr.ReTrainRecognizer();
            
            //var res = fr.RecognizeFace(Image.FromFile(@"c:\temp\1.jpg"));
            
            //Console.WriteLine(sw.ElapsedMilliseconds);
            //Console.WriteLine(fr.SaveFace(0,0, Image.FromFile(fName)));
            //fr.GetImages(0);
            //fr.RecognizeFace(Image.FromFile(fName));
            //Console.ReadKey();
            //Test();
            //refresh();
            //recognize();
            features();
            //UploadFAces();
            //delete();
        }
        private static void delete()
        {
            FaceRecognition fr = new FaceRecognition();
            //fr.DeletePictureFromDB(2, 4);
            fr.ReTrainRecognizer();
        }
        private static void features()
        {            
            //Console.WriteLine(PictureQuality.CheckPicture(Image.FromFile(@"c:\temp\1.jpg")));
            //string s = "IMAG0072";
            //Image x = Image.FromFile(@"d:\"+s+".jpg");
            Image x = Image.FromFile(@"c:\temp\1.jpg");
            PictureQuality.GetPictureFeatures(ref x);
            x.Save(@"c:\temp\1t.jpg", ImageFormat.Jpeg);
            
        }

        private static void recognize()
        {
            //get all files in list
            Stopwatch s = new Stopwatch();
            string filename = @"c:\RECFOLDER";
            List<string> lsFiles = new List<string>();
            DirectoryInfo d = new DirectoryInfo(filename);
            Orion.ImageProcessing.FaceRecognition rec = new FaceRecognition();
            int brojTocnih = 0;
            int iFalsePos = 0;
            foreach (var f in d.EnumerateFiles())
            {
                s.Start();
                Image fileToRec = Image.FromFile(f.FullName);
                Orion.ImageProcessing.RecognitionResult res = rec.RecognizeFace(fileToRec);
                Console.WriteLine(string.Format("{0} recognized as {1} with {2}%", f.Name, res.Id, res.Confidence));
                if (res.Id.ToString() != f.Name.Substring(0, 1) && res.Confidence > 50)
                {
                    Console.WriteLine(string.Format("{0} recognized as {1} with {2}%", f.Name, res.Id, res.Confidence));
                    //WriteLog(string.Format("{0} recognized as {1} with {2}%", file, res.Id, res.Confidence));
                    iFalsePos++;
                }
                else if (res.Confidence > 50)
                {
                    brojTocnih++;
                    Console.WriteLine(string.Join("\n", res.MatchResults));
                }
                s.Stop();
                Console.WriteLine("Elapsed time: " + s.Elapsed.ToString());
                s.Reset();
                //if false add to result
                Console.ReadKey();
                Console.WriteLine("__________________________________________________________________________");
            }
            Console.WriteLine(string.Format("tocno pogodjeno {0} od ukupno {1} prepoznavanja, FalsePozitivniih {2}", brojTocnih, lsFiles.Count, iFalsePos));
            Console.ReadKey();
        }

        private static void refresh()
        {
            //Stopwatch stw = new Stopwatch();
            //stw.Start();
            //FaceRecognition rec = new FaceRecognition();
            //rec.RefreshTemplate(0);
            //stw.Stop();
            //Console.WriteLine(string.Format("pictures trained in {0}", stw.Elapsed.ToString()));
            //stw.Reset();
            //Console.ReadKey();
            string s = @"C:\temp\train\0.template";
            string x = Path.GetFileNameWithoutExtension(s);
            Console.WriteLine(x);
            Console.ReadKey();

        }

        private static void Test()
        {
            //get all files in list

            Stopwatch stw = new Stopwatch();

            string filename = @"c:\FaceDBTEST";
            Dictionary<int, List<string>> dFiles = new Dictionary<int, List<string>>();
            List<string> lsFiles = new List<string>();
            DirectoryInfo d = new DirectoryInfo(filename);
            Orion.ImageProcessing.FaceRecognition rec;
            int brojTocnih = 0;
            int iFalsePos = 0;

            foreach (var dir in d.EnumerateDirectories())
            {
                List<string> files = new List<string>();
                foreach (var f in dir.EnumerateFiles())
                {
                    files.Add(f.FullName);
                    lsFiles.Add(f.FullName);
                }
                dFiles.Add(Convert.ToInt32(dir.Name), files);
            }
            //for all files (train for all but i)
            //foreach (string file in lsFiles)
            //{
                //clean train folder
                //List<string> filesToDelete = new List<string>();
                //filesToDelete.AddRange(Directory.GetFiles(@"C:\temp\train", "*.*").ToList<string>());

                //deleteFiles(filesToDelete);
                //train recognizer                
                for (int i = 0; i < dFiles.Keys.Count; i++)
                {       
                    int brojilo = 0;
                    List<Image> slike = new List<Image>();
                    foreach (string s in dFiles[i])
                    {
                        //if (s == file)
                        //    continue;
                        Image fileToSend = Image.FromFile(s);

                        slike.Add(fileToSend);
                        brojilo++;
                    }
                    stw.Start();
                    rec = new FaceRecognition();
                    rec.SaveFace(i, brojilo, slike);
                    
                    stw.Stop();
                    Console.WriteLine(string.Format("{0} pictures trained in {1}", brojilo, stw.Elapsed.ToString()));
                    stw.Reset();
                }
                rec = new FaceRecognition();
                rec.RefreshTemplate();
                Console.ReadKey();
                                
                //try to recognize i
            //    Image fileToRec = Image.FromFile(file);
            //    rec = new FaceRecognition();
            //    Orion.ImageProcessing.RecognitionResult res = rec.RecognizeFace(fileToRec);
            //    Console.WriteLine(string.Format("{0} recognized as {1} with {2}%", file, res.Id, res.Confidence));
            //    if (res.Id.ToString() != file.Substring(14, 1) && res.Confidence > 50)
            //    {
            //        Console.WriteLine(string.Format("{0} recognized as {1} with {2}%", file, res.Id, res.Confidence));
            //        WriteLog(string.Format("{0} recognized as {1} with {2}%", file, res.Id, res.Confidence));
            //        iFalsePos++;
            //    }
            //    else if (res.Confidence > 50)
            //    {
            //        brojTocnih++;
            //        WriteLog(string.Join("\n", res.MatchResults));
            //    }
            //    //if false add to result
            //}
            //WriteLog(string.Format("tocno pogodjeno {0} od ukupno {1} prepoznavanja, FalsePozitivniih {2}", brojTocnih, lsFiles.Count, iFalsePos));
        }

        private static void UploadFAces()
        {
            string filename = @"c:\FaceDB2";
            //Image fileToSend;
            DirectoryInfo d = new DirectoryInfo(filename);
            
            //foreach (var dir in d.EnumerateDirectories())
            //{
            int brojilo = 0;
            List<string> files = new List<string>();
            foreach (var f in d.EnumerateFiles())
            {
                if (f.Name.Contains("ra") || f.Name.Contains("sb") || f.Name.Contains("qd") || f.Name.Contains("ta") || f.Name.Contains("ua") || f.Name.Contains("vc") || f.Name.Contains("wa") || f.Name.Contains("xb") || f.Name.Contains("xe") || f.Name.Contains("ya") || f.Name.Contains("zb") || f.Name.Contains("zd"))
                    continue;
                files.Add(f.FullName);
            }
            int iFC = files.Count;
            int iFCC = 1;

            Orion.ImageProcessing.FaceRecognition rec = new FaceRecognition();
            for (int i = 0; i <= 629; i++)
            {
                brojilo = 0;
                var res = files.Where(w => w.Contains(i.ToString("000")));
                foreach (var subject in res)
                {
                    Image fileToSend = Image.FromFile(subject);
                    if (!PictureQuality.CheckPicture(fileToSend))
                        continue;
                    rec.SaveFace(i, brojilo, fileToSend);

                    Console.Write(String.Format("\r{0,3}% - {1,5}/{2,5}", (int)(100*iFCC/iFC), iFCC, iFC));
                    iFCC++;
                    brojilo++;
                }
            }
        }
        static void deleteFiles(List<string> files)
        {
            foreach (string s in files)
            {
                File.Delete(s);
            }
        }
        public static void WriteLog(String message)
        {
            //just in case: we protect code with try.
            try
            {
                string filename = "log_1";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(filename, true);
                XElement xmlEntry = new XElement("logEntry",
                    new XElement("Date", System.DateTime.Now.ToString()),
                    new XElement("Message", message));
                sw.WriteLine(xmlEntry);
                sw.Close();
            }
            catch (Exception)
            {
            }
        }
    }
}
